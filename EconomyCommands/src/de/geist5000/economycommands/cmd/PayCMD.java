package de.geist5000.economycommands.cmd;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.geist5000.economycommands.utils.StringManager;
import de.geist5000.economycommands.utils.Strings;
import de.geist5000.gamefliegerapi.global.data.GameFliegerAPI;
import de.geist5000.gamefliegerapi.global.data.PlayerData;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class PayCMD extends Command {

	public PayCMD() {
		super("pay");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		GameFliegerAPI api = (GameFliegerAPI) ProxyServer.getInstance().getPluginManager().getPlugin("GameFliegerAPI");
		// Bukkit: GameFliegerAPI api = (GameFliegerAPI)
		// Bukkit.getPluginManager().getPlugin("GameFliegerAPI");
		StringManager stringManager = new StringManager();

		if (sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			if (p.hasPermission("command.pay")) {
				if (args.length == 2) {

					if (api.contains(p.getUniqueId())) {
						try {
							PlayerData player1Data = api.getPlayerData(p.getUniqueId());
							String input = args[0];
							Double amount = Double.parseDouble(args[1].replace(',', '.'));
							List<String> names = new ArrayList<>();
							
							
							if (input.equalsIgnoreCase("all") || input.equalsIgnoreCase("*")) {
								names = p.getServer().getInfo().getPlayers().stream().filter(o ->( api.contains(o.getUniqueId()) && o != p)).map(o -> o.getName()).collect(Collectors.toList());
							} else {
								names.add(input);
							}

							for (String name : names) {
								if (api.contains(name)) {

									PlayerData player2Data = api.getPlayerData(name);

									if (amount <= player1Data.getCoins()) {
										player1Data.subtractCoins(amount);
										player2Data.addCoins(amount);
										p.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.INFO_MONEY_TRANSFERRED, name, api, amount)));
										ProxiedPlayer reciever = ProxyServer.getInstance().getPlayer(name);

										if (reciever != null) {
											reciever.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.INFO_MONEY_GOT, p.getDisplayName(), api, amount)));
										}
									} else {
										p.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NOT_ENOUGH_MONEY)));
									}

								} else {
									p.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NEVER_SEEN)));
								}
							}

						} catch (NumberFormatException e) {
							p.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_NUMBER)));
						}
					} else {
						sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_INVALID_PERFORMER)));
					}
				} else {
					p.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_PAY_INFO)));
				}
			} else {
				p.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_PERMISSION)));
			}

		}

	}

}
