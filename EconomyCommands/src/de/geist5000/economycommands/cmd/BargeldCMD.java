package de.geist5000.economycommands.cmd;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.geist5000.economycommands.utils.StringManager;
import de.geist5000.economycommands.utils.Strings;
import de.geist5000.gamefliegerapi.global.data.Column;
import de.geist5000.gamefliegerapi.global.data.GameFliegerAPI;
import de.geist5000.gamefliegerapi.global.data.PlayerData;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BargeldCMD extends Command {

	public BargeldCMD() {
		super("bargeld");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		GameFliegerAPI api = (GameFliegerAPI) ProxyServer.getInstance().getPluginManager().getPlugin("GameFliegerAPI");
		// Bukkit: GameFliegerAPI api = (GameFliegerAPI)
		// Bukkit.getPluginManager().getPlugin("GameFliegerAPI");

		StringManager stringManager = new StringManager();
		if (args.length > 0) {

			if (args.length == 3) {

				if (args[0].equalsIgnoreCase("add")) {
					if (sender.hasPermission("command.bargeld.add")) {
						try {
							String input = args[1];
							Double amount = getAmountFromString(args[2]);
							List<String> names = new ArrayList<>();
							if (input.equalsIgnoreCase("all") || input.equalsIgnoreCase("*")) {
								if (sender instanceof ProxiedPlayer) {
									ProxiedPlayer p = (ProxiedPlayer) sender;
									names = p.getServer().getInfo().getPlayers().stream().map(o -> o.getName()).collect(Collectors.toList());
								} else {
									sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_PLAYER)));
								}
							} else {
								names.add(input);
							}
							for (String name : names) {
								if (api.contains(name)) {
									PlayerData playerData = api.getPlayerData(name);
									playerData.addCoins(amount);
									sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.INFO_MONEY_ADDED, name, api, amount)));
									ProxiedPlayer p = ProxyServer.getInstance().getPlayer(name);
									if (p != null) {
										p.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.INFO_MONEY_RECIEVED)));
									}
								} else {
									sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NEVER_SEEN)));
								}
							}

						} catch (NumberFormatException e) {
							sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_NUMBER)));
						}
					} else {
						sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_PERMISSION)));
					}
				}

				if (args[0].equalsIgnoreCase("remove")) {
					if (sender.hasPermission("command.bargeld.remove")) {
						try {
							String input = args[1];
							Double amount = getAmountFromString(args[2]);
							List<String> names = new ArrayList<>();
							if (input.equalsIgnoreCase("all") || input.equalsIgnoreCase("*")) {
								if (sender instanceof ProxiedPlayer) {
									ProxiedPlayer p = (ProxiedPlayer) sender;
									names = p.getServer().getInfo().getPlayers().stream().map(o -> o.getName()).collect(Collectors.toList());
								} else {
									sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_PLAYER)));
								}
							} else {
								names.add(input);
							}

							for (String name : names) {
								if (api.contains(name)) {
									PlayerData playerData = api.getPlayerData(name);
									playerData.subtractCoins(amount);
									sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.INFO_MONEY_REMOVED, name, api, amount)));
								} else {
									sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NEVER_SEEN)));
								}
							}

						} catch (NumberFormatException e) {
							sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_NUMBER)));
						}
					} else {
						sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_PERMISSION)));
					}
				}

				if (args[0].equalsIgnoreCase("set")) {
					if (sender.hasPermission("command.bargeld.set")) {
						try {
							String input = args[1];
							Double amount = getAmountFromString(args[2]);
							List<String> names = new ArrayList<>();
							if (input.equalsIgnoreCase("all") || input.equalsIgnoreCase("*")) {
								if (sender instanceof ProxiedPlayer) {
									ProxiedPlayer p = (ProxiedPlayer) sender;
									names = p.getServer().getInfo().getPlayers().stream().map(o -> o.getName()).collect(Collectors.toList());
								} else {
									sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_PLAYER)));
								}
							} else {
								names.add(input);
							}
							for (String name : names) {
								if (api.contains(name)) {
									PlayerData playerData = api.getPlayerData(name);
									playerData.setCoins(amount);
									sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.INFO_MONEY_SET, name, api, amount)));
								} else {
									sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NEVER_SEEN)));
								}
							}
							

						} catch (NumberFormatException e) {
							sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_NUMBER)));
						}
					} else {
						sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_PERMISSION)));
					}
				}

			}

			if (args.length == 2) {

				if (args[0].equalsIgnoreCase("top")) {
					if (sender.hasPermission("command.bargled.top")) {
						try {
							Integer page = Integer.parseInt(args[1]);
							List<String> names = api.getScoreboardFromName(Column.COINS, 10 * (page - 1), 10 * page);
							Integer i = 1;
							for (String name : names) {
								PlayerData playerData = api.getPlayerData(name);
								sender.sendMessage(
										TextComponent.fromLegacyText(stringManager.getString(Strings.INFO_SCOREBOARD_TOP, name, api, playerData.getCoins()).replaceAll("%score%", i.toString())));
								i++;
							}
						} catch (NumberFormatException e) {
							sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_NUMBER)));
						}
					} else {
						sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_PERMISSION)));
					}
				}
			}

			if (args.length == 1) {

				if (args[0].equalsIgnoreCase("top")) {
					if (sender.hasPermission("command.bargled.top")) {
						try {
							Integer page = 1;
							List<String> names = api.getScoreboardFromName(Column.COINS, 10 * (page - 1), 10 * page);
							Integer i = 10 * (page - 1) + 1;
							for (String name : names) {
								PlayerData playerData = api.getPlayerData(name);
								sender.sendMessage(
										TextComponent.fromLegacyText(stringManager.getString(Strings.INFO_SCOREBOARD_TOP, name, api, playerData.getCoins()).replaceAll("%score%", i.toString())));
								i++;
							}
						} catch (NumberFormatException e) {
							sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_NUMBER)));
						}
					} else {
						sender.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.ERR_NO_PERMISSION)));
					}
				}

				if (args[0].equalsIgnoreCase("help")) {
					List<String> helpMessage = stringManager.getStringList(Strings.ERR_BARGELD_INFO);
					for (String string : helpMessage) {
						sender.sendMessage(TextComponent.fromLegacyText(string));
					}
				}

			}

		} else {

			if (sender instanceof ProxiedPlayer) {
				ProxiedPlayer p = (ProxiedPlayer) sender;

				if (api.contains(p.getUniqueId())) {
					PlayerData playerData = api.getPlayerData(p.getUniqueId());
					p.sendMessage(TextComponent.fromLegacyText(stringManager.getString(Strings.INFO_OWN_MONEY, p.getDisplayName(), api, playerData.getCoins())));
				}
			}
		}
	}
	
	private static Double getAmountFromString(String string) {
		string = string.replaceAll(",", ".");
		Double amount = Double.parseDouble(string);
		Double answer = ((int) (amount*100))/100D;
		return answer;
	}

}
