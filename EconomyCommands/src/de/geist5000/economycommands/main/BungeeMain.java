package de.geist5000.economycommands.main;

import de.geist5000.economycommands.cmd.BargeldCMD;
import de.geist5000.economycommands.cmd.PayCMD;
import de.geist5000.economycommands.utils.StringManager;
import net.md_5.bungee.api.plugin.Plugin;

public class BungeeMain extends Plugin{
	
	
	
	@Override
	public void onEnable() {
		super.onEnable();
		getProxy().getPluginManager().registerCommand(this, new BargeldCMD());
		getProxy().getPluginManager().registerCommand(this, new PayCMD());
		new StringManager();
	}

}
