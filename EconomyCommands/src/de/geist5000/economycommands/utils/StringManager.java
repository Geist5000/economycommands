package de.geist5000.economycommands.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.geist5000.gamefliegerapi.global.data.GameFliegerAPI;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class StringManager {

	private Configuration config;
	private File file = new File("plugins/EconomyCommands", "Strings.yml");
	private String lng;
	private String prefix;

	public StringManager() {
			if(!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			
			if(!file.exists()) {
				Configuration defaultConfig = new Configuration();
				defaultConfig.set("Language", "de");
				defaultConfig.set("Prefix", "[Economy]");
				defaultConfig.set("de." + Strings.INFO_OWN_MONEY, "Du besitzt %amount%");
				defaultConfig.set("de." + Strings.INFO_MONEY_RECIEVED, "Geld bekommen %amount%");
				defaultConfig.set("de." + Strings.INFO_MONEY_REMOVED, "Geld entfernt %amount%");
				defaultConfig.set("de." + Strings.INFO_MONEY_ADDED, "Geld hinzugefügt %amount%");
				defaultConfig.set("de." + Strings.INFO_MONEY_SET, "Geld gesetzt %amount%");
				defaultConfig.set("de." + Strings.INFO_SCOREBOARD_TOP, "%score%: %player%-%amount%");
				defaultConfig.set("de." + Strings.INFO_MONEY_TRANSFERRED, "Du hast %player% %amount% geschickt");
				defaultConfig.set("de." + Strings.INFO_MONEY_GOT, "Du hast %amount% von %player% bekommen");
				defaultConfig.set("de." + Strings.ERR_NO_NUMBER, "Keine nummer");
				defaultConfig.set("de." + Strings.ERR_NEVER_SEEN, "Nie gesehen");
				defaultConfig.set("de." + Strings.ERR_NO_PERMISSION, "Keine Permission");
				defaultConfig.set("de." + Strings.ERR_INVALID_PERFORMER, "Du darfst das nicht");
				defaultConfig.set("de." + Strings.ERR_NOT_ENOUGH_MONEY, "Du hast nicht genug Geld");
				defaultConfig.set("de." + Strings.ERR_PAY_INFO, "/pay [Name] [Amount]");
				defaultConfig.set("de." + Strings.ERR_NO_PLAYER, "Kein Spieler");
				defaultConfig.set("de." + Strings.ERR_BARGELD_INFO, Arrays.asList(
						"/bargeld | Dein Geld" , 
						"/bargeld set [Name] [Amount] | setzt Geld",
						"/bargeld add [Name] [Amount] | fügt Geld hinzu",
						"/bargeld remove [Name] [Amount] | entfernt Geld", 
						"/bargeld top [page(optional)] | zeigt das Scoreboard an"));
				
				
				save(defaultConfig);
			}
			
			try {
				config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
				lng = config.getString("Language");
				prefix = config.getString("Prefix");
				
			} catch (IOException e) {
				System.out.println(e);
			}
		}

	

	

	public String getString(Strings stringType , String name , GameFliegerAPI api , Double amount) {
		String string = prefix + " " + config.getString(lng + "." + stringType , "");
		if(amount != null && api != null) {
			string = string.replaceAll("%amount%", api.format(amount));
		}
		
		if(name != null && (!name.equalsIgnoreCase(""))) {
			string = string.replaceAll("%player%", name);
		}
		return string;
	}
	
	public String getString(Strings stringType) {
		return getString(stringType, null, null , null);
	}
	
	public List<String> getStringList(Strings stringType) {
		return config.contains(lng + "." + stringType) ? config.getStringList(lng + "." + stringType) : new ArrayList<>();
	}
	
	private void save(Configuration config) {
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, file);
		} catch (IOException e) {
			System.out.println(e);
		}
	}





	

}
